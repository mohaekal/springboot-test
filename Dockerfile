FROM openjdk:8-jdk-alpine
RUN apk update && apk add wget && rm -rf /var/cache/apk/*
RUN wget --user=admin --password=Admin123$%^ https://nexus.oc.indolinux.com/repository/karyawan-teladan/com/itgroup/karyawan-teladan/v1.8/karyawan-teladan-v1.8.jar
EXPOSE 8080
CMD ["java", "-jar", "karyawan-teladan-v1.8.jar"]
